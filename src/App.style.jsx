import styled from 'styled-components';

export const Container = styled.div`
  top: 9vh;
  position: absolute;
  width: 100vw;
  max-height: 72vh;
  min-height: 91vh;
  overflow-x: hidden;
  font-familty: 'Roboto';
  left: 0;
`;
