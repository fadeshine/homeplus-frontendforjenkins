import React from 'react';
import { Box, Container, Row, Column, FooterLink, Heading, Bottom } from './Footer.style.jsx';

const Footer = () => (
  <Box>
    <Container>
      <Row>
        <Column>
          <Heading href="#">About Us</Heading>
          <Heading href="#">How It Works</Heading>
        </Column>
        <Column>
          <Heading>Our location</Heading>
          <FooterLink href="#">30 Victoria St., NSW 3000</FooterLink>
        </Column>
        <Column>
          <Heading>Contact Us</Heading>
          <FooterLink href="#">+61 000 000 000</FooterLink>
        </Column>
        <Column>
          <Heading>
            {' '}
            <br />
          </Heading>
          <FooterLink href="#">service@homeplus.com</FooterLink>
        </Column>
      </Row>
    </Container>
    <Bottom>Copyright&copy; HomePlus Pty. Ltd 2022-Present, All rights reserved</Bottom>
  </Box>
);
export default Footer;
