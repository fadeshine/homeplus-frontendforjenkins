import React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

const TaskerInput = ({ title }) => {
  return (
    <Box sx={{ display: 'flex', flexDirection: 'row', margin: '30px' }}>
      <Typography sx={{ display: 'flex', marginRight: '20px', width: '150px' }}>{title}</Typography>
      <TextField id="outlined-basic" size="small" sx={{ width: '450px' }} />
    </Box>
  );
};

TaskerInput.propTypes = {
  title: PropTypes.string.isRequired,
};

export default TaskerInput;
