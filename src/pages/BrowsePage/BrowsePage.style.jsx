import styled from 'styled-components';

export const ToolBar = styled.div`
  display: flex;
  width: 80%;
  align-items: center;
  margin: auto;
  justify-content: start;
  padding-top: 50px;
  position: relative;
  form,
  > button,
  > div {
    margin-left: 50px;
  }
`;

export const Contents = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
  padding-top: 30px;
  gap: 20px;
  width: 75%;
  margin: auto;
`;

export const LocationBox = styled.div`
  width: 200px;
  height: 240px;
  background-color: #fff;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  border-radius: 5px;
  position: absolute;
  left: 22rem;
  z-index: 99;
  top: 95%;
  display: flex;
  flex-direction: column;
  font-family: roboto;
  align-items: center;
  padding: 1rem;
  div {
    margin-top: 0.3rem;
  }
  .distanceBox {
    width: 75%;
    display: flex;
    align-items: center;
    label {
      text-align: center;
    }
    justify-content: space-between;
  }
`;
