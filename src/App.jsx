import React, { Suspense } from 'react';

import LoadingPage from './components/LoadingPage';
import Header from './components/Navbar';
import Route from './routes';

import { Container } from './App.style';

const App = () => {
  return (
    <>
      <Header />
      <Container>
        <Suspense fallback={<LoadingPage />}>
          <Route />
        </Suspense>
      </Container>
    </>
  );
};

export default App;
